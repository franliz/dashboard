import flask, json
from flask import request, jsonify
from flask_cors import CORS
import sqlite3

app = flask.Flask(__name__)
CORS(app)
app.config["DEBUG"] = True

@app.route('/', methods=['GET'])
def home():
    return '''<h1>Xenitt Dashboard</h1>
<p>Una API prototipo para leer a distancia datos de Xenitt Dashboard</p>'''

@app.errorhandler(404)
def page_not_found(e):
    return "<h1>404</h1><p>The resource could not be found.</p>", 404

@app.route('/api/v1/resources/sensors_post', methods=['POST'])
def api_sensor_post():
    conn = sqlite3.connect('db.sqlite3')
    cur = conn.cursor()
    info = request.data
    finaldata = json.loads(info)
    idquery = """SELECT id_uuid FROM 'sensor';"""
    cur.execute(idquery)
    records = cur.fetchall()
    idlist = []
    for x in records:
        idlist.append(x[0])
    for x in finaldata:
        if x['id_uuid'] in idlist:
                row = """UPDATE sensor 
                SET measured_on = ?
                WHERE id_uuid = ?;"""
                data_tuple = (x['max'], x['id_uuid'])
                cur.execute(row, data_tuple)
                conn.commit()
        else:
                row = """INSERT INTO 'sensor' 
                ('client', 'id_uuid', 'sensor_label', 'measured_on') 
                VALUES (?, ?, ?, ?);"""
                data_tuple = (x['client'], x['id_uuid'], x['sensor_label'], x['max'])
                cur.execute(row, data_tuple)
                conn.commit()
    return info

@app.route('/api/v1/resources/clients', methods=['GET'])
def api_get_client():
    query_parameters = request.args
    name = query_parameters.get('name')
    query = """SELECT name, end_contract FROM 'company' WHERE db_name=(?) ORDER BY name ASC;"""

    conn = sqlite3.connect('db.sqlite3')
    cur = conn.cursor()
    cur.execute(query, (name,))
    client_data = cur.fetchall()
    client_list = []

    for x in client_data:
        client = {
        'name': x[0],
        'date': x[1]
        }
        client_list.append(client)

    return jsonify(client_list)

@app.route('/api/v1/resources/sensorlist', methods=['GET'])
def api_client_filter():
    query_parameters = request.args
    client = query_parameters.get('client')
    query = """SELECT measured_on, sensor_label FROM 'sensor' WHERE client=(?) ORDER BY sensor_label ASC;"""

    conn = sqlite3.connect('db.sqlite3')
    cur = conn.cursor()
    cur.execute(query, (client,))
    filtered_sensors = cur.fetchall()
    sensor_list = []
    for x in filtered_sensors:
        sensor = {
            'measured_on': x[0],
            'sensor_label': x[1]}
        sensor_list.append(sensor)
    return jsonify(sensor_list)

app.run(debug=True, port=8080)
