from flask import Blueprint, render_template, redirect, url_for, request, flash
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import login_user, logout_user, login_required
from flask_mail import Message
from .models import User, Organization
from . import db
auth = Blueprint('auth', __name__)

@auth.route('/login')
def login():
    return render_template('login.html')

@auth.route('/login', methods=['POST'])
def login_post():
    email = request.form.get('email')
    password = request.form.get('password')
    remember = True if request.form.get('remember') else False

    user = User.query.filter_by(email=email).first()
    
    if not user or not check_password_hash(user.password, password):
        flash('Por favor, revisa tus datos e intenta de nuevo.')
        return redirect(url_for('auth.login'))
    
    login_user(user, remember=remember)

    return redirect(url_for('main.panel'))

@auth.route('/signup')
def signup():
    orgdata = Organization.query.all()
    return render_template('signup.html', organizations=orgdata)

@auth.route('/signup', methods=['POST'])
def signup_post():
    email = request.form.get('email')
    name = request.form.get('name')
    last_name = request.form.get('lastname')
    password = request.form.get('password')
    organization_id = request.form.get('organization')

    user = User.query.filter_by(email=email).first()

    if user:
        flash('El correo electrónico ya se encuentra registrado.')
        return redirect(url_for('auth.signup'))
    else:
        new_user = User(email=email, password=generate_password_hash(password, method='sha256'), name=name, last_name=last_name, organization_id=organization_id)
        db.session.add(new_user)
        db.session.commit()
    
    return redirect(url_for('auth.login'))

@auth.route('/logout')
@login_required
def logout():
    logout_user()
    return render_template('logout.html')

@auth.route('/recover')
def recover():
    return render_template('recoverpw.html')

@auth.route('/recover', methods=['POST'])
def recover_post():
    destination = request.form.get('email')
    user = User.query.filter_by(email=destination).first()
    if not destination:
        flash('Por favor, ingresa un email.')
        return redirect(url_for('auth.recover'))
    elif not user:
        flash('El usuario no se encuentra registrado.')
        return redirect(url_for('auth.recover'))
    else:
        return render_template('confirm-mail.html', email=destination)